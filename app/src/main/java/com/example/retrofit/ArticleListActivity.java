package com.example.retrofit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.example.retrofit.adapter.ArticleListAdapter;
import com.example.retrofit.callback.OnSuccessCallBack;
import com.example.retrofit.data.data.api.api.Services.repository.ArticleRepository;
import com.example.retrofit.data.data.api.api.response.AllArticleResponse;

public class ArticleListActivity extends AppCompatActivity {

    private AllArticleResponse dataSet;
    private RecyclerView rvArticle;
    private ArticleListAdapter adapter;
    private ArticleRepository articleRepository;
    private ProgressBar pbLoading;
    private SwipeRefreshLayout pullRefresh;

    final static String TAG = "Maintain";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_list);

        // init recyclerview
        rvArticle = findViewById(R.id.rv_article);
        pbLoading = findViewById(R.id.pb_loading);
        pullRefresh = findViewById(R.id.pull_refresh);

        pullRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.d(TAG,"on REFRESH");
                fetchData();
            }
        });

        //init article repo
        articleRepository = new ArticleRepository();
        fetchData();
    }
    private void setupRecyclerView() {

            adapter = new ArticleListAdapter(this, dataSet);
            rvArticle.setAdapter(adapter);
            rvArticle.setLayoutManager(new LinearLayoutManager(this));
            adapter.notifyDataSetChanged();

    }
    private void fetchData() {
        dataSet = articleRepository.findAllArticle(1, 15, new OnSuccessCallBack() {
            @Override
            public void onSuccess() {
                setupRecyclerView();
                pbLoading.setVisibility(View.GONE);
                pullRefresh.setRefreshing(false);
            }
        });
    }
}
