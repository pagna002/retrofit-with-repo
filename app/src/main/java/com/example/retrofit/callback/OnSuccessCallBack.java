package com.example.retrofit.callback;

public interface OnSuccessCallBack {
    void onSuccess();
}
