package com.example.retrofit.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.retrofit.R;
import com.example.retrofit.data.data.api.api.response.AllArticleResponse;

public class ArticleListAdapter extends RecyclerView.Adapter<ArticleListAdapter.ArticleViewHolder> {

    private Context context;
    private AllArticleResponse dataSet;

    public ArticleListAdapter(Context context, AllArticleResponse dataSet){
        this.context= context;
        this.dataSet = dataSet;
    }

    @NonNull
    @Override
    public ArticleListAdapter.ArticleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.article_item_layout,parent,false);

        return new ArticleViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ArticleListAdapter.ArticleViewHolder holder, int position) {
        holder.textTitle.setText(dataSet.getData().get(position).getTitle());
        holder.textDesc.setText(dataSet.getData().get(position).getDesc());
        Glide.with(context).load(dataSet.getData().get(position).getImageUri()).placeholder(R.drawable.placeholder).into(holder.imageArticle);
    }

    @Override
    public int getItemCount() {
        return dataSet.getData().size();
    }


    class ArticleViewHolder extends RecyclerView.ViewHolder{
        TextView textTitle, textDesc;
        ImageView imageArticle;

        public ArticleViewHolder(@NonNull View itemView) {
            super(itemView);
            textTitle = itemView.findViewById(R.id.tv_title);
            textDesc = itemView.findViewById(R.id.tv_desc);
            imageArticle = itemView.findViewById(R.id.image_person);
        }
    }
}
