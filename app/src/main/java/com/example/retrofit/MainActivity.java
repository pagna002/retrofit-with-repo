package com.example.retrofit;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import com.example.retrofit.data.data.ServiceGenerator;
import com.example.retrofit.data.data.api.api.Services.ArticleService;
import com.example.retrofit.data.data.api.api.response.AllArticleResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {


    private final static String TAG = "MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArticleService articleService =
                ServiceGenerator.createServices(ArticleService.class);

        Call<AllArticleResponse> callArticle =  articleService.findAll(1,5);

        //background
        callArticle.enqueue(new Callback<AllArticleResponse>() {
            @Override
            public void onResponse(Call<AllArticleResponse> call, Response<AllArticleResponse> response) {
               if (response.isSuccessful()&& response.body()!=null){
                   AllArticleResponse allArticleResponse =
                           response.body();
                   Log.d(TAG, "onResponse : " + allArticleResponse.getData().get(0));
               }
            }
            @Override
            public void onFailure(Call<AllArticleResponse> call, Throwable t) {
                Log.e(TAG, "onFailure : " + t.getMessage());
            }
        });



    }
}
