package com.example.retrofit.data.data.api.api.Services;

import com.example.retrofit.data.data.api.api.response.AllArticleResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ArticleService {

    @GET ("v1/api/articles")
    Call<AllArticleResponse> findAll(@Query("page")long page,
                                     @Query("limit") long limit);

}
