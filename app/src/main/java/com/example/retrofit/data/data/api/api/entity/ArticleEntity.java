package com.example.retrofit.data.data.api.api.entity;

import com.google.gson.annotations.SerializedName;

public class ArticleEntity {

    @SerializedName("ID")
    private int id;
    @SerializedName("TITLE")
    private  String title;
    @SerializedName("DESCRIPTION")
    private  String desc;
    @SerializedName("IMAGE")
    private String imageUri;

    public ArticleEntity(int id, String title, String desc, String imageUri) {
        this.id = id;
        this.title = title;
        this.desc = desc;
        this.imageUri = imageUri;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }

    @Override
    public String toString() {
        return "ArticleEntity{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", desc='" + desc + '\'' +
                ", imageUri='" + imageUri + '\'' +
                '}';
    }
}
