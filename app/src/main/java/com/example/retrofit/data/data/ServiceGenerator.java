package com.example.retrofit.data.data;

import com.example.retrofit.data.data.api.api.BaseUrlConfig;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {
    //create retrofit.builder object
    private static Retrofit.Builder builder =  new Retrofit.Builder()
            .baseUrl(BaseUrlConfig.BASE_URL_AMS)
            .addConverterFactory(GsonConverterFactory.create());

   //create retrofit object
    private static Retrofit retrofit = builder.build();

    //create service class
    public static <S> S createServices (Class<S>  serviceClass){
        return retrofit.create(serviceClass);
    }
}
