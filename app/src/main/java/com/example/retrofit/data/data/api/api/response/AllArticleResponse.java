package com.example.retrofit.data.data.api.api.response;

import com.example.retrofit.data.data.api.api.entity.ArticleEntity;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AllArticleResponse {

    @SerializedName("CODE")
    private String code;
    @SerializedName("MESSAGE")
    private String message;
    @SerializedName("DATA")
    List<ArticleEntity> data;

    @Override
    public String toString() {
        return "AllArticleResponse{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ArticleEntity> getData() {
        return data;
    }

    public void setData(List<ArticleEntity> data) {
        this.data = data;
    }
}
