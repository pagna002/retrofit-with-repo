package com.example.retrofit.data.data.api.api.Services.repository;

import android.util.Log;

import com.example.retrofit.callback.OnSuccessCallBack;
import com.example.retrofit.data.data.ServiceGenerator;
import com.example.retrofit.data.data.api.api.Services.ArticleService;
import com.example.retrofit.data.data.api.api.response.AllArticleResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticleRepository {
    private  final static String TAG = "Article";
    private ArticleService articleService;

public ArticleRepository(){
    articleService = ServiceGenerator.createServices(ArticleService.class);
}


public AllArticleResponse findAllArticle(int page, int limit, final OnSuccessCallBack callBack) {
    final AllArticleResponse dataSet = new AllArticleResponse();

    articleService.findAll(page, limit).enqueue(new Callback<AllArticleResponse>() {
        @Override
        public void onResponse(Call<AllArticleResponse> call, Response<AllArticleResponse> response) {
            if (response.isSuccessful()){
                Log.e(TAG, "onRespone : Successfull");
                dataSet.setCode(response.body().getCode());
                dataSet.setMessage(response.body().getMessage());
                dataSet.setData(response.body().getData());
                callBack.onSuccess();
            }
        }

        @Override
        public void onFailure(Call<AllArticleResponse> call, Throwable t) {
            Log.e(TAG, "onFailure : " + t.getMessage());
        }
    });
return dataSet;
}


}
